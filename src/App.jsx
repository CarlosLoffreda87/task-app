import React from "react";
import Form from "./components/form/Form";
import TaskReact from "./components/task/TaskReact";
import { useState } from "react";
import "./App.css";

function App() {
  const [tasks, setTask] = useState([]);

  function createTask(task) {
    setTask([
      ...tasks,
      {
        number: tasks.length + 1,
        title: task.title,
        description: task.description,
      },
    ]);
  }

  function deleteTask(taskNumber) {
    setTask(tasks.filter((task) => task.number !== taskNumber));
  }

  function deleteAllTasks() {
    const deleteConfirm = window.confirm("Se borrarán todas las tareas")
    if (deleteConfirm){
      setTask([])
    }
  }

  return (
    <div>
      <Form createTask={createTask} />
      <br />
      <hr />
      <TaskReact
        tasks={tasks}
        deleteTask={deleteTask}
        deleteAllTasks={deleteAllTasks}
      />
    </div>
  );
}

export default App;
