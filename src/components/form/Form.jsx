import React from "react";
import "./Form.css";
import { useState } from "react";

function Form({ createTask }) {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();

    if (title.length == 0 && description.length == 0) {
      alert("Debes completar los campos antes de guardar las tareas");
    } else if (title.length == 0) {
      alert("El campo Título no puede estar vacío");
    } else if (description.length == 0) {
      alert("El campo Descripción no puede estar vacío");
    } else {
      if (document.getElementById("checkbox").checked !== false) {
        createTask({
          title,
          description,
        });
        setTitle("");
        setDescription("");
        document.getElementById("checkbox").checked = true;
      } else {
        alert("Debes aceptar los términos y condiciones!");
      }
    }
  };
  return (
    <div className="formContainer">
      <form onSubmit={handleSubmit}>
        <div className="title">
          <label htmlFor="name">Título:</label>
          <input
            type="text"
            name="name"
            id="name"
            placeholder="Escribe el título de tu tarea..."
            onChange={(e) => setTitle(e.target.value)}
            value={title}
          />
        </div>
        <div className="description">
          <label>Descripción:</label>
          <textarea
            name="description"
            id=""
            cols="30"
            rows="6"
            placeholder="Escribe la descripción de tu tarea..."
            onChange={(e) => setDescription(e.target.value)}
            value={description}
          ></textarea>
        </div>
        <div className="button">
          <div className="terms">
            <p>Acepto los términos y condiciones</p>
            <input type="checkbox" className="check" id="checkbox" />
          </div>
          <button>Guardar Tarea</button>
        </div>
      </form>
    </div>
  );
}

export default Form;
