import React from "react";
import "./Task.css"

function TaskCard({ task, deleteTask }) {
  const eliminar = () => {
    alert(`Estás seguro de eliminar la tarea número ${task.number}?`);
    deleteTask(task.number);
  };

  return (
    <div className="task">
      <div className="pinContainer">
        <img src="src/components/images/purplePin.png" alt="pinImage" />
      </div>
      <h4>Numero de Tarea: {task.number}</h4>
      <hr />
      <h4>Título de Tarea: {task.title}</h4>
      <hr />
      <h4>Descripción de Tarea: {task.description}</h4>
      <hr />
      <button onClick={eliminar}>Eliminar Tarea</button>
    </div>
  );
}

export default TaskCard;
