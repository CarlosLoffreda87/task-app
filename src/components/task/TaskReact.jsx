import TaskCard from "../card/TaskCard";
import "./taskReact.css";

function TaskReact({ tasks, deleteTask, deleteAllTasks }) {
  if (tasks.length === 0) {
    return (
      <div className="h2Container">
        <h2>No hay tareas por el momento...😌</h2>
      </div>
    );
  }

  const deteleAll = ()=> {
    deleteAllTasks()
  }


  return (
    <>
      <div className="deleteAllBUtton">
        <button onClick={deteleAll} className="redButton">Elinimar todas las tareas</button>
      </div>
      <div className="container">
        {tasks.map((task) => (
          <TaskCard task={task} key={task.number} deleteTask={deleteTask} />
        ))}
      </div>
    </>
  );
}

export default TaskReact;
